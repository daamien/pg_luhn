BEGIN;

CREATE EXTENSION luhn;

CREATE SCHEMA tests;
CREATE EXTENSION pg_doctest WITH SCHEMA tests;

SELECT tests.passing('luhn');

-- uncomment to debug the doctests
--SELECT tests.test_schema('luhn');

ROLLBACK;
