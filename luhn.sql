
DROP SCHEMA IF EXISTS luhn CASCADE;
CREATE SCHEMA luhn;

CREATE OR REPLACE FUNCTION luhn.checksum(TEXT)
RETURNS numeric
LANGUAGE SQL
AS $$
  -- >>> SELECT luhn.checksum('972487086');
  -- 50
  -- >>> SELECT luhn.checksum('927487086');
  -- 54
  -- >>> SELECT luhn.checksum(NULL);
  -- NULL
  SELECT SUM(luhn_transform)
  FROM (
    SELECT
      CASE WHEN ROW_NUMBER() OVER (ORDER BY position DESC) % 2 != 0
      THEN digit
      ELSE (digit*2) % 9
      END AS luhn_transform
  FROM (
    SELECT digit::INT, ROW_NUMBER() OVER () as position
    FROM regexp_split_to_table($1, '') as digit
    ) AS split_number_into_digit
) AS compute_luhn_value ;
$$;


CREATE OR REPLACE FUNCTION luhn.generate(TEXT)
RETURNS numeric
LANGUAGE SQL
AS $$
    -- >>> SELECT luhn.generate('35693803564380');
    -- 8
    -- >>> SELECT luhn.generate('53461861341123');
    -- 4
   SELECT 10 - (luhn.checksum($1 || '0') % 10)
$$;


CREATE OR REPLACE FUNCTION luhn.verify(TEXT)
RETURNS BOOLEAN
LANGUAGE SQL
AS $$
  -- >>> SELECT luhn.verify('972487086');
  -- true
  -- >>> SELECT luhn.verify('927487086');
  -- false
  SELECT luhn.checksum($1)%10 = 0;
$$;

CREATE OR REPLACE FUNCTION luhn.append(TEXT)
RETURNS TEXT
LANGUAGE SQL
AS $$
  -- >>> SELECT luhn.append('35693803564380')
  -- 356938035643808
  SELECT $1 || luhn.generate($1);
$$;

