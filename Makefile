EXTENSION = luhn
EXTENSION_VERSION=$(shell grep default_version $(EXTENSION).control | sed -e "s/default_version[[:space:]]*=[[:space:]]*'\([^']*\)'/\1/")
DATA = $(EXTENSION)--$(EXTENSION_VERSION).sql

# Use this var to add more tests
#PG_TEST_EXTRA ?= ""
REGRESS_TESTS = pg_doctest 
REGRESS_TESTS+=$(PG_TEST_EXTRA)
# This can be overridden by an env variable
REGRESS?=$(REGRESS_TESTS)
REGRESS_OPTS = --inputdir=tests

# postgres build stuff
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

all: extension

##
## B U I L D
##
.PHONY: extension
extension: $(DATA)#: build the extension

$(DATA): $(EXTENSION).sql
	cp $^ $@

##
## D O C K E R
##
DOCKER_IMAGE?=pgxn/pgxn-tools

docker_bash: #: enter the docker image (useful for testing)
	docker run -it --rm --volume "`pwd`:/source" --workdir /source --user "$(id -u):$(id -g)" $(DOCKER_IMAGE) bash


